package com.sephora.happyshop;

import com.sephora.happyshop.category.interactor.ProductListInteractorImpl;
import com.sephora.happyshop.common.webservice.Model.Product;
import com.sephora.happyshop.common.webservice.request.ProductApi;
import com.sephora.happyshop.common.webservice.request.ProductWebService;
import com.sephora.happyshop.common.webservice.response.ProductListResponse;

import org.greenrobot.eventbus.EventBus;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;

import retrofit.RestAdapter;

/**
 * Created by Farhana on 5/19/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class ProductListInteractorTest {
    private static final String BASE_URL = "http://sephora-mobile-takehome-apple.herokuapp.com/api/v1";
    private static final String CATEGORY = "category";
    private static final String PRODUCT_NAME = "name";
    private static final double PRODUCT_PRICE = 10.00;
    private static final String PRODUCT_URL = "URL";
    private static final long PRODUCT_ID = 11 ;


    @Mock
    ProductWebService productWebService;
    @Mock
    EventBus eventBus;
    ProductListInteractorImpl productListInteractor;



    @Test
    public void loadItemsTest(){
        productListInteractor = new ProductListInteractorImpl(productWebService, eventBus);
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(BASE_URL)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        ProductApi productApi = restAdapter.create(ProductApi.class);
        Product product = new Product();
        product.setCategory(CATEGORY);
        product.setId(PRODUCT_ID);
        product.setName(PRODUCT_NAME);
        product.setImgUrl(PRODUCT_URL);
        product.setPrice(PRODUCT_PRICE);
        product.setUnderSale(true);
        ArrayList<Product> products = new ArrayList<>();

        products.add(product);
        ProductListResponse productListResponse = new ProductListResponse();
        productListResponse.setProducts(products);
        Mockito.when(productWebService.getProductApi()).thenReturn(productApi);
    }


}
