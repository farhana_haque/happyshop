package com.sephora.happyshop;

import com.sephora.happyshop.category.interactor.IProductListInteractor;
import com.sephora.happyshop.category.presenter.ProductListPresenterImpl;
import com.sephora.happyshop.category.ui.view.IProductListFragmentView;

import org.greenrobot.eventbus.EventBus;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

/**
 * Created by Farhana on 5/19/2016.
 */
public class ProductListPresenterTest {
    @Mock
    IProductListFragmentView view;
    @Mock
    IProductListInteractor interactor;
    @Mock
    EventBus eventBus;
    ProductListPresenterImpl productListPresenter;
    @Before
    public void setUP() {
        productListPresenter = new ProductListPresenterImpl(view, interactor, eventBus);
    }

    @Test
    public void shouldLoadError(){

    }

}

