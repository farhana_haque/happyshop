package com.sephora.happyshop.home.presenter;

import android.os.Bundle;

import com.sephora.happyshop.common.callback.OnDataLoadFinishedListener;
import com.sephora.happyshop.common.webservice.Model.Product;
import com.sephora.happyshop.home.interactor.CartInteractorImpl;
import com.sephora.happyshop.home.interactor.ICartInteractor;
import com.sephora.happyshop.home.ui.view.ICartView;

import java.util.List;

/**
 * Created by Farhana on 5/19/2016.
 */
public class CartPresenterImpl implements ICartPresenter, OnDataLoadFinishedListener {
    ICartView cartView;

    ICartInteractor cartInteractor;
    public CartPresenterImpl(ICartView cartView) {
        this.cartView = cartView;
        cartInteractor = new CartInteractorImpl();

    }

    @Override
    public <T> void onLoadingFinish(T response) {
        List<Product> products = (List<Product>) response;
        if (products.size() == 0) {
            cartView.showNoProductLayout();
        } else {
            cartView.setData((List<Product>) response);
        }

    }

    @Override
    public void onLoadingError() {

    }

    @Override
    public void loadData() {
        cartInteractor.findItems(this);

    }

    @Override
    public void onItemClick(int position) {
        cartView.navigateToProductDetails(position);

    }

    @Override
    public void onViewCreated(Bundle bundle) {
        loadData();

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

    }

    @Override
    public void onSaveInstantState(Bundle bundle) {

    }
}
