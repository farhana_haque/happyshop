package com.sephora.happyshop.home.interactor;

import com.sephora.happyshop.App;
import com.sephora.happyshop.common.callback.OnDataLoadFinishedListener;
import com.sephora.happyshop.common.storage.database.DBManager;

/**
 * Created by Farhana on 5/19/2016.
 */
public class CartInteractorImpl implements ICartInteractor {

    @Override
    public void findItems(OnDataLoadFinishedListener dataLoadFinishedListener) {
        DBManager dbManager = DBManager.getInstance(App.getContext());
        dataLoadFinishedListener.onLoadingFinish(dbManager.getProducts());
    }
}
