package com.sephora.happyshop.home.interactor;

/**
 * Created by Farhana on 5/17/2016.
 */
public interface ICategoryListInteractor {
    void findCategories();
}
