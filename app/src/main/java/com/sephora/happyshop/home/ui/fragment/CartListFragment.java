package com.sephora.happyshop.home.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sephora.happyshop.R;
import com.sephora.happyshop.common.adapter.RecyclerItemClickListener;
import com.sephora.happyshop.common.base.BaseFragment;
import com.sephora.happyshop.common.webservice.Model.Product;
import com.sephora.happyshop.home.adapter.CartRecycleViewAdapter;
import com.sephora.happyshop.home.presenter.CartPresenterImpl;
import com.sephora.happyshop.home.ui.view.ICartView;
import com.sephora.happyshop.product.ui.activity.ProductDetailsActivity;

import java.util.List;

import butterknife.BindView;


public class CartListFragment extends BaseFragment implements ICartView {

    public static final String FRAGMENT_TAG = "CartListFragment";

    @BindView(R.id.recycler_view)
    RecyclerView cartRecyclerView;
    private GridLayoutManager gridLayoutManager;
    CartRecycleViewAdapter cartRecycleViewAdapter;
    CartPresenterImpl presenter;

    public CartListFragment() {
        // Required empty public constructor
    }


    public static CartListFragment newInstance() {
        CartListFragment fragment = new CartListFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void initPresenter() {
        presenter = new CartPresenterImpl(this);
    }


    @Override
    protected View getView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_cart_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        gridLayoutManager = new GridLayoutManager(getActivity(), getResources().getInteger(R.integer.max_product_in_grid));
        cartRecyclerView.setLayoutManager(gridLayoutManager);

        cartRecycleViewAdapter = new CartRecycleViewAdapter(getActivity());
        cartRecyclerView.setAdapter(cartRecycleViewAdapter);
        cartRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                presenter.onItemClick(position);
            }
        }));
        presenter.onViewCreated(savedInstanceState
        );
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.loadData();
    }

    @Override
    public void setData(List<Product> products) {
        cartRecycleViewAdapter.setProducts(products);
        cartRecycleViewAdapter.notifyDataSetChanged();
    }

    @Override
    public void showNoProductLayout() {

    }

    @Override
    public void hideNoProductLayout() {

    }

    @Override
    public void navigateToProductDetails(int itemIndex) {
        Intent intent = new Intent(getActivity(), ProductDetailsActivity.class);
        Product product = cartRecycleViewAdapter.getItem(itemIndex);
        if(product != null) {
            intent.putExtra(ProductDetailsActivity.PRODUCT, (Parcelable) product);
        }
        getActivity().startActivity(intent);

    }
}
