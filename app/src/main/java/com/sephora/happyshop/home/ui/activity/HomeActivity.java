package com.sephora.happyshop.home.ui.activity;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.widget.TextView;

import com.sephora.happyshop.R;
import com.sephora.happyshop.common.base.BaseActivity;
import com.sephora.happyshop.home.ui.fragment.CartListFragment;
import com.sephora.happyshop.home.ui.fragment.CategoryListFragment;
import com.sephora.happyshop.home.ui.fragment.SettingsFragment;
import com.sephora.happyshop.home.ui.view.IHomeView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;


public class HomeActivity extends BaseActivity implements IHomeView {


    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.tabs) TabLayout tabLayout;
    @BindView(R.id.viewpager) ViewPager viewPager;


    @Override
    protected void setupLayout() {
        setContentView(R.layout.activity_home);
    }

    @Override
    protected void initView() {
/*        toolbar = (Toolbar) findViewById(R.id.toolbar);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);*/
        setupActionBar();
        setupViewPager();

        setupTabs();

    }

    @Override
    public void setupActionBar() {
        setSupportActionBar(toolbar);
    }

    @Override
    public void setupViewPager() {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(CategoryListFragment.newInstance(), CategoryListFragment.FRAGMENT_TAG);
        adapter.addFrag(CartListFragment.newInstance(), CartListFragment.FRAGMENT_TAG);
        adapter.addFrag(SettingsFragment.newInstance(), SettingsFragment.FRAGMENT_TAG);
        viewPager.setAdapter(adapter);

    }

    /**
     * Adding custom view to tab
     */
    @Override
    public void setupTabs() {
        tabLayout.setupWithViewPager(viewPager);
        TextView tabOne = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabOne.setText(getString(R.string.tab_category_title));

        tabOne.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_category, 0, 0);
        tabLayout.getTabAt(0).setCustomView(tabOne);

        TextView tabTwo = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabTwo.setText(getString(R.string.tab_cart_title));
        tabTwo.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_cart, 0, 0);
        tabLayout.getTabAt(1).setCustomView(tabTwo);

        TextView tabThree = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabThree.setText(getString(R.string.tab_settings_title));
        tabThree.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_settings, 0, 0);
        tabLayout.getTabAt(2).setCustomView(tabThree);
    }

    /**
     * Adding fragments to ViewPager
     *
     * @param viewPager
     */
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(CategoryListFragment.newInstance(), CategoryListFragment.FRAGMENT_TAG);
        adapter.addFrag(CartListFragment.newInstance(), CartListFragment.FRAGMENT_TAG);
        adapter.addFrag(SettingsFragment.newInstance(), SettingsFragment.FRAGMENT_TAG);
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
