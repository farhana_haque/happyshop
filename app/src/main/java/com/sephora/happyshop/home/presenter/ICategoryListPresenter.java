package com.sephora.happyshop.home.presenter;

import com.sephora.happyshop.common.base.IFragmentPresenter;

/**
 * Created by Farhana on 5/17/2016.
 */
public interface ICategoryListPresenter extends IFragmentPresenter {
    void loadCategories();
    void categoriesItemClick(int itemIndex);
}
