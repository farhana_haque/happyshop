package com.sephora.happyshop.home.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sephora.happyshop.R;
import com.sephora.happyshop.category.adapter.ProductRecyclerViewHolder;
import com.sephora.happyshop.common.webservice.Model.Product;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Farhana on 5/17/2016.
 */
public class CartRecycleViewAdapter extends RecyclerView.Adapter<ProductRecyclerViewHolder> {
    private List<Product> products;
    private Context context;

    public CartRecycleViewAdapter(Context context) {
        products = new ArrayList<>();
        this.context = context;
    }

    @Override
    public ProductRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_list_item_layout, null);
        ProductRecyclerViewHolder viewHolder = new ProductRecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ProductRecyclerViewHolder holder, int position) {
        Product product = products.get(position);
        ProductRecyclerViewHolder productRecyclerViewHolder =  holder;
        productRecyclerViewHolder.tvProductName.setText(product.getName());
        productRecyclerViewHolder.tvProductPrice.setText(context.getString(R.string.currency)+product.getPrice());
        if(product.isUnderSale()){
            productRecyclerViewHolder.tvProductOnSale.setVisibility(View.VISIBLE);
        }else{
            productRecyclerViewHolder.tvProductOnSale.setVisibility(View.GONE);
        }

        if(product.getImgUrl() != null) {
            Uri uri = Uri.parse(product.getImgUrl());
            Picasso.with(context).load(uri).into(productRecyclerViewHolder.imageViewProduct);
        }

    }



    @Override
    public int getItemCount() {
        return this.products.size();
    }

    public void setProducts(List<Product> products){
        this.products = products;
    }

    public Product getItem(int itemIndex) {
        if(itemIndex >= 0 && itemIndex < getItemCount()){
            return products.get(itemIndex);
        }
        return null;
    }
}
