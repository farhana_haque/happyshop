package com.sephora.happyshop.home.interactor;

import com.sephora.happyshop.common.event.LoadCategoryEvent;
import com.sephora.happyshop.common.webservice.Model.Category;

import org.greenrobot.eventbus.EventBus;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Farhana on 5/17/2016.
 */
public class CategoryListInteractorImpl implements ICategoryListInteractor {
    EventBus eventBus = EventBus.getDefault();

    @Override
    public void findCategories() {

        eventBus.post(prepareCategoryData());
    }

    private LoadCategoryEvent prepareCategoryData() {
        List<Category> categories = Arrays.asList(new Category("Bath & Body"), new Category("Makeup"), new Category("Skincare"), new Category("Nails"), new Category("Tools"), new Category("Men"));
        LoadCategoryEvent loadCategoryEvent = new LoadCategoryEvent(categories);
        return loadCategoryEvent;

    }
}
