package com.sephora.happyshop.home.ui.view;

import com.sephora.happyshop.common.webservice.Model.Category;

import java.util.List;

/**
 * Created by Farhana on 5/16/2016.
 */
public interface ICategoryListFragmentView {
    void setData(List<Category> categories);
    void showNoDataLayout();
    void showDataLoadErrorMessage(String message);
    void notifyDataChange();
    void navigateToCategoryDetails(int itemIndex);
    boolean isViewAttached();
}
