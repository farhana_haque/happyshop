package com.sephora.happyshop.home.presenter;

import com.sephora.happyshop.common.base.IFragmentPresenter;

/**
 * Created by Farhana on 5/19/2016.
 */
public interface ICartPresenter extends IFragmentPresenter{
    void loadData();
    void onItemClick(int position);
}
