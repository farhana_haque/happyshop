package com.sephora.happyshop.home.ui.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sephora.happyshop.R;
import com.sephora.happyshop.category.ui.activity.CategoryActivity;
import com.sephora.happyshop.common.adapter.RecyclerItemClickListener;
import com.sephora.happyshop.common.base.BaseFragment;
import com.sephora.happyshop.common.webservice.Model.Category;
import com.sephora.happyshop.home.adapter.CategoryRecycleViewAdapter;
import com.sephora.happyshop.home.interactor.CategoryListInteractorImpl;
import com.sephora.happyshop.home.interactor.ICategoryListInteractor;
import com.sephora.happyshop.home.presenter.CategoryListPresenterImpl;
import com.sephora.happyshop.home.ui.view.ICategoryListFragmentView;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.BindView;

public class CategoryListFragment extends BaseFragment implements ICategoryListFragmentView {

    public static final String FRAGMENT_TAG = "CategoryListFragment";

    @BindView(R.id.recycler_view) RecyclerView categoryRecyclerView ;
    LinearLayoutManager linearLayoutManager;
    CategoryRecycleViewAdapter categoryRecycleViewAdapter;
    CategoryListPresenterImpl presenter;
    EventBus eventBus = EventBus.getDefault();
    ICategoryListInteractor categoryListInteractor;


    public CategoryListFragment() {

    }

    public static CategoryListFragment newInstance() {
        CategoryListFragment fragment = new CategoryListFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    protected void initPresenter() {
        categoryListInteractor = new CategoryListInteractorImpl();
        presenter = new CategoryListPresenterImpl(this, categoryListInteractor, eventBus);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        categoryRecyclerView.setLayoutManager(linearLayoutManager);

        categoryRecycleViewAdapter = new CategoryRecycleViewAdapter(getActivity());
        categoryRecyclerView.setAdapter(categoryRecycleViewAdapter);
        categoryRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                presenter.categoriesItemClick(position);
            }
        }));
        presenter.loadCategories();

    }

    @Override
    protected View getView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_category, container, false);
    }

/*
    public void initView() {
        categoryRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);

        linearLayoutManager = new LinearLayoutManager(getActivity());
    }*/

    @Override
    public void setData(List<Category> categories) {
        categoryRecycleViewAdapter.setCategories(categories);
        categoryRecycleViewAdapter.notifyDataSetChanged();

    }

    @Override
    public void showNoDataLayout() {

    }

    @Override
    public void showDataLoadErrorMessage(String message) {

    }

    @Override
    public void notifyDataChange() {

    }

    @Override
    public void navigateToCategoryDetails(int itemIndex) {
        Intent intent = new Intent(getActivity(), CategoryActivity.class);
        Category category = categoryRecycleViewAdapter.getItem(itemIndex);
        if(category != null) {
            intent.putExtra(CategoryActivity.EXTRA_CATEGORY, category.getName());
        }
        getActivity().startActivity(intent);
    }

    @Override
    public boolean isViewAttached() {
        return isAdded();
    }
/*
    @Override
    public <T> void onItemClick(T item) {

    }*/
}
