package com.sephora.happyshop.home.ui.view;

/**
 * Created by Farhana on 5/14/2016.
 */
public interface IHomeView {
    void setupActionBar();
    void setupViewPager();
    void setupTabs();
}
