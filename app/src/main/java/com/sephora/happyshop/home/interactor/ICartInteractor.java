package com.sephora.happyshop.home.interactor;

import com.sephora.happyshop.common.callback.OnDataLoadFinishedListener;

/**
 * Created by Farhana on 5/19/2016.
 */
public interface ICartInteractor {
    void findItems(OnDataLoadFinishedListener dataLoadFinishedListener);
}
