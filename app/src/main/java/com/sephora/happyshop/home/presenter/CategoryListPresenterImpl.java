package com.sephora.happyshop.home.presenter;

import android.os.Bundle;

import com.sephora.happyshop.common.event.LoadCategoryEvent;
import com.sephora.happyshop.home.interactor.ICategoryListInteractor;
import com.sephora.happyshop.home.ui.view.ICategoryListFragmentView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * Created by Farhana on 5/17/2016.
 */
public class CategoryListPresenterImpl implements ICategoryListPresenter{

    ICategoryListFragmentView categoryListView;
    ICategoryListInteractor categoryListInteractor;


    EventBus eventBus;
    public CategoryListPresenterImpl(ICategoryListFragmentView categoryListView, ICategoryListInteractor interactor, EventBus eventBus) {
        this.categoryListView = categoryListView;
        this.categoryListInteractor = interactor;//new CategoryListInteractorImpl();
        this.eventBus = eventBus;
    }

    @Override
    public void onViewCreated(Bundle bundle) {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

    }

    @Override
    public void onSaveInstantState(Bundle bundle) {

    }


    public void loadCategories(){
        eventBus.register(this);
        categoryListInteractor.findCategories();

    }

    @Override
    public void categoriesItemClick(int itemIndex) {
        categoryListView.navigateToCategoryDetails(itemIndex);

    }
    @Subscribe
    public void onLoadFinish(LoadCategoryEvent loadCategoryEvent){
        eventBus.unregister(this);
        categoryListView.setData(loadCategoryEvent.getCategories());
    }

 /*   @Override
    public <T> void onLoadingFinish(List<T> items) {
        categoryListView.setData((List<Category>) items);

    }*/

  /*  @Override
    public <T> void onLoadingFinish(T response) {
        categoryListView.setData((List<Category>) response);
    }

    @Override
    public void onLoadingError() {

    }*/
}
