package com.sephora.happyshop.home.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sephora.happyshop.R;
import com.sephora.happyshop.common.webservice.Model.Category;
import com.sephora.happyshop.home.adapter.CategoryRecyclerViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Farhana on 5/17/2016.
 */
public class CategoryRecycleViewAdapter extends RecyclerView.Adapter<CategoryRecyclerViewHolder> {
    private List<Category> categories;
    private Context context;

    public CategoryRecycleViewAdapter(Context context) {
        categories = new ArrayList<>();
        this.context = context;
    }

    @Override
    public CategoryRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.catagory_list_item_layout, null);
        CategoryRecyclerViewHolder viewHolder = new CategoryRecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CategoryRecyclerViewHolder holder, int position) {
        holder.categoryName.setText(categories.get(position).getName());

    }

    @Override
    public int getItemCount() {
        return this.categories.size();
    }

    public void setCategories(List<Category> categories){
        this.categories = categories;
    }

    public Category getItem(int itemIndex) {
        if(itemIndex >= 0 && itemIndex < getItemCount()){
            return categories.get(itemIndex);
        }
        return null;
    }
}
