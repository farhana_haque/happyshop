package com.sephora.happyshop.home.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.sephora.happyshop.R;

/**
 * Created by Farhana on 5/17/2016.
 */
public class CategoryRecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    //@BindView(R.id.nameTextView)
    public TextView categoryName;

    public CategoryRecyclerViewHolder(View itemView) {
        super(itemView);
        categoryName = (TextView) itemView.findViewById(R.id.nameTextView);

    }

    @Override
    public void onClick(View view) {

    }
}
