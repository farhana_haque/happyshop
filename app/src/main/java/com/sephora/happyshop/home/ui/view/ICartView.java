package com.sephora.happyshop.home.ui.view;

import com.sephora.happyshop.common.webservice.Model.Product;

import java.util.List;

/**
 * Created by Farhana on 5/19/2016.
 */
public interface ICartView {
    void setData(List<Product> products);
    void showNoProductLayout();
    void hideNoProductLayout();
    void navigateToProductDetails(int itemIndex);

}
