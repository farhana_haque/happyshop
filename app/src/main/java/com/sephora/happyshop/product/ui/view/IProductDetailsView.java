package com.sephora.happyshop.product.ui.view;

import com.sephora.happyshop.common.webservice.Model.Product;
import com.sephora.happyshop.common.webservice.Model.ProductDetails;

/**
 * Created by Farhana on 5/19/2016.
 */
public interface IProductDetailsView {
    void setProductDetails(ProductDetails productDetails);
    void showProductLoadErrorMessage();
    void setInitialProductDetails(Product product);
    void showAlreadyAddedToCartMessage();
    void showAddedToCartMessage();
    void showErrorOnSaveToCartMessage();
    void showAddToCart();
    void showRemoveFromCart();
    ProductDetails getProductDetails();
    boolean isAttachedView();
}
