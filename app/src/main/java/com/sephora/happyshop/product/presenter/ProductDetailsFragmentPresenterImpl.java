package com.sephora.happyshop.product.presenter;

import android.os.Bundle;

import com.sephora.happyshop.common.event.AddToCartFailedEvent;
import com.sephora.happyshop.common.event.AddToCartSuccessEvent;
import com.sephora.happyshop.common.event.AlreadyExistsEvent;
import com.sephora.happyshop.common.event.AlreadyInCartEvent;
import com.sephora.happyshop.common.event.ProductDetailsLoadErrorEvent;
import com.sephora.happyshop.common.event.ProductDetailsLoadSuccessEvent;
import com.sephora.happyshop.common.event.RemoveFromCartSuccessEvent;
import com.sephora.happyshop.common.webservice.Model.Product;
import com.sephora.happyshop.common.webservice.Model.ProductDetails;
import com.sephora.happyshop.product.interactor.IProductDetailsInteractor;
import com.sephora.happyshop.product.ui.view.IProductDetailsView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * Created by Farhana on 5/19/2016.
 */
public class ProductDetailsFragmentPresenterImpl implements IProductDetailsFragmentPresenter{
    private static final String PRODUCT_DETAILS = "product_details";
    IProductDetailsView productDetailsView;
    IProductDetailsInteractor productDetailsInteractor;
    EventBus eventBus;
    private boolean restoredProductDetails = false;


    public ProductDetailsFragmentPresenterImpl(IProductDetailsView productDetailsView, IProductDetailsInteractor productDetailsInteractor, EventBus eventBus) {
        this.productDetailsView = productDetailsView;
        this.productDetailsInteractor = productDetailsInteractor;
        this.eventBus = eventBus;
    }

    @Override
    public void onViewCreated(Bundle savedInstanceState) {
        if(savedInstanceState != null){
            ProductDetails productDetails = savedInstanceState.getParcelable(PRODUCT_DETAILS);
            if(productDetails != null){
                restoredProductDetails = true;
                productDetailsView.setProductDetails(productDetails);
            }
        }
   /*     if(restoredProductDetails == false) {
            Product product = (Product) bundle.getSerializable(ProductDetailsFragment.PRODUCT);
            productDetailsView.setInitialProductDetails(product);
            registerEventBus();
            productDetailsInteractor.checkIsExists(product.getId());
            productDetailsInteractor.findProducts(product.getId());
        }*/

    }

    private void registerEventBus() {
        if(eventBus.isRegistered(this) == false){
            eventBus.register(this);
        }
    }

    private void unRegisterEventBus(){
        if(eventBus.isRegistered(this)){
            eventBus.unregister(this);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

    }

    @Override
    public void onSaveInstantState(Bundle bundle) {
        bundle.putParcelable(PRODUCT_DETAILS, productDetailsView.getProductDetails());
    }

    @Subscribe
    public void onLoadingFinish(ProductDetailsLoadSuccessEvent productDetailsLoadSuccessEvent) {
        unRegisterEventBus();
        productDetailsView.setProductDetails(productDetailsLoadSuccessEvent.getProductDetails());


    }

    @Subscribe
    public void onLoadingError(ProductDetailsLoadErrorEvent loadErrorEvent) {
        unRegisterEventBus();
        productDetailsView.showProductLoadErrorMessage();

    }

    @Override
    public void findProductDetails(Product product) {
        if(restoredProductDetails == false) {
            productDetailsView.setInitialProductDetails(product);
            registerEventBus();
           // productDetailsInteractor.checkIsExists(product.getId());
            productDetailsInteractor.findProducts(product.getId());
        }

    }

    @Override
    public void onClickAddToCart(Product product) {
        registerEventBus();
        productDetailsInteractor.addProductIntoCart(product);

    }

    @Override
    public void onClickDeteleFromCart(long id) {
        registerEventBus();
        productDetailsInteractor.deleteProduct(id);
    }

    @Override
    public void checkIsAlreadyInCart(long id) {
        registerEventBus();
        productDetailsInteractor.checkIsExists(id);

    }

    @Subscribe
    public void addedSuccessfully(AddToCartSuccessEvent addToCartSuccessEvent) {
        unRegisterEventBus();
        productDetailsView.showAddedToCartMessage();
        productDetailsView.showRemoveFromCart();

    }

    @Subscribe
    public void errorOnAdd(AddToCartFailedEvent addToCartFailedEvent) {
        unRegisterEventBus();
        productDetailsView.showErrorOnSaveToCartMessage();
    }

    @Subscribe
    public void alreadyAdded(AlreadyInCartEvent alreadyInCartEvent) {
        unRegisterEventBus();
        productDetailsView.showAlreadyAddedToCartMessage();

    }

    @Subscribe
    public void isExists(AlreadyExistsEvent alreadyExistsEvent) {
        if(alreadyExistsEvent.isExists()){
            productDetailsView.showRemoveFromCart();
        }else{
            productDetailsView.showAddToCart();
        }
    }

    @Subscribe
    public void deletedSuccessfully(RemoveFromCartSuccessEvent removeFromCartSuccessEvent) {
        unRegisterEventBus();
        productDetailsView.showAddToCart();

    }

    @Subscribe
    public void errorOnDelete() {
        unRegisterEventBus();

    }
}
