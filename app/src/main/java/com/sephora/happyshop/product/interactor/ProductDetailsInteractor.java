package com.sephora.happyshop.product.interactor;

import android.util.Log;

import com.sephora.happyshop.App;
import com.sephora.happyshop.common.event.AddToCartFailedEvent;
import com.sephora.happyshop.common.event.AddToCartSuccessEvent;
import com.sephora.happyshop.common.event.AlreadyExistsEvent;
import com.sephora.happyshop.common.event.AlreadyInCartEvent;
import com.sephora.happyshop.common.event.ProductDetailsLoadErrorEvent;
import com.sephora.happyshop.common.event.ProductDetailsLoadSuccessEvent;
import com.sephora.happyshop.common.event.RemoveFromCartFailedEvent;
import com.sephora.happyshop.common.event.RemoveFromCartSuccessEvent;
import com.sephora.happyshop.common.storage.Constant.ProductPersistStatusConstants;
import com.sephora.happyshop.common.storage.database.DBManager;
import com.sephora.happyshop.common.webservice.Model.Product;
import com.sephora.happyshop.common.webservice.request.ProductWebService;
import com.sephora.happyshop.common.webservice.response.ProductDetailsResponse;

import org.greenrobot.eventbus.EventBus;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Farhana on 5/19/2016.
 */
public class ProductDetailsInteractor implements IProductDetailsInteractor {
    public static final String PRODUCT_TYPE_CART = "cart";
    private final String PATH_VARIABLE_SUFFIX = ".json";
    private final String TAG = ProductDetailsInteractor.class.getSimpleName();
    ProductWebService productWebService;
    EventBus eventBus;

    public ProductDetailsInteractor(EventBus eventBus, ProductWebService productWebService) {
        this.productWebService = productWebService;
        this.eventBus = eventBus;
    }

    @Override
    public void findProducts(long id) {
        productWebService.getProductApi()
                .getProduct(id+PATH_VARIABLE_SUFFIX)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ProductDetailsResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, e.getMessage());
                        eventBus.post(new ProductDetailsLoadErrorEvent("error"));
                       // finishedListener.onLoadingError();

                    }

                    @Override
                    public void onNext(ProductDetailsResponse productDetailsResponse) {
                        eventBus.post(new ProductDetailsLoadSuccessEvent(productDetailsResponse.getProduct()));
                       // finishedListener.onLoadingFinish(productDetailsResponse.getProduct());
                    }
                });
    }

    @Override
    public void addProductIntoCart(Product product) {
        DBManager dbManager = DBManager.getInstance(App.getContext());
        int saveStatus = dbManager.saveProduct(product, PRODUCT_TYPE_CART);
        if(saveStatus == ProductPersistStatusConstants.SAVED_SUCCESSFULLY){
            eventBus.post(new AddToCartSuccessEvent());
           // listener.addedSuccessfully();
        }else if(saveStatus == ProductPersistStatusConstants.ALREADY_EXISTS){
            eventBus.post(new AlreadyInCartEvent());
            //listener.alreadyAdded();
        }else {
            eventBus.post(new AddToCartFailedEvent());
            //listener.errorOnAdd();
        }
    }

    @Override
    public void checkIsExists(long id) {
        DBManager dbManager = DBManager.getInstance(App.getContext());
        eventBus.post(new AlreadyExistsEvent(dbManager.checkProductExists(id)));
       // onDatabaseExistsListener.isExists(dbManager.checkProductExists(id));
    }

    @Override
    public void deleteProduct(long id) {
        DBManager dbManager = DBManager.getInstance(App.getContext());
        boolean isDeleted = dbManager.deleteProducts(id);
        if(isDeleted){
            eventBus.post(new RemoveFromCartSuccessEvent());
            //onDatabaseDeleteListener.deletedSuccessfully();
        }else{
            eventBus.post(new RemoveFromCartFailedEvent());
           // onDatabaseDeleteListener.errorOnDelete();
        }
    }
}
