package com.sephora.happyshop.product.ui.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.sephora.happyshop.R;
import com.sephora.happyshop.common.base.BaseActivity;
import com.sephora.happyshop.common.webservice.Model.Product;
import com.sephora.happyshop.product.ui.fragment.ProductDetailsFragment;

import butterknife.BindView;

public class ProductDetailsActivity extends BaseActivity{

    public static final String PRODUCT = "product";

    @BindView(R.id.toolbar) Toolbar toolbar;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState == null){
            Fragment fragment = ProductDetailsFragment.newInstance(getProduct());
            getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, fragment).commit();
        }
    }

    @Override
    protected void setupLayout() {
        setContentView(R.layout.activity_product_details);
    }

    @Override
    protected void initView() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getBackNavigationTitle());

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private Product getProduct(){
        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            return bundle.getParcelable(PRODUCT);
        }
        return null;
    }

    private String getBackNavigationTitle(){
        Product product = getProduct();
        if(product != null){
            return product.getCategory();
        }
        return "";
    }

}
