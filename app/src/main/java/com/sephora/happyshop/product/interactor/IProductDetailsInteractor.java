package com.sephora.happyshop.product.interactor;

import com.sephora.happyshop.common.webservice.Model.Product;

/**
 * Created by Farhana on 5/19/2016.
 */
public interface IProductDetailsInteractor {
    void findProducts(long id);
    void addProductIntoCart(Product product);
    void checkIsExists(long id);
    void deleteProduct(long id);
}
