package com.sephora.happyshop.product.presenter;

import com.sephora.happyshop.common.base.IFragmentPresenter;
import com.sephora.happyshop.common.webservice.Model.Product;

/**
 * Created by Farhana on 5/19/2016.
 */
public interface IProductDetailsFragmentPresenter extends IFragmentPresenter {
    void findProductDetails(Product product);
    void onClickAddToCart(Product product);
    void onClickDeteleFromCart(long id);
    void checkIsAlreadyInCart(long id);
}
