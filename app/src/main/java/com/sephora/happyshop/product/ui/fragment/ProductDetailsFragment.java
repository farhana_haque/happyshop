package com.sephora.happyshop.product.ui.fragment;


import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.sephora.happyshop.R;
import com.sephora.happyshop.common.base.BaseFragment;
import com.sephora.happyshop.common.webservice.Model.Product;
import com.sephora.happyshop.common.webservice.Model.ProductDetails;
import com.sephora.happyshop.common.webservice.request.ProductWebService;
import com.sephora.happyshop.common.webservice.util.CheckNetworkConnection;
import com.sephora.happyshop.product.interactor.IProductDetailsInteractor;
import com.sephora.happyshop.product.interactor.ProductDetailsInteractor;
import com.sephora.happyshop.product.presenter.ProductDetailsFragmentPresenterImpl;
import com.sephora.happyshop.product.ui.view.IProductDetailsView;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.OnClick;


public class ProductDetailsFragment extends BaseFragment implements IProductDetailsView{

    public static final String PRODUCT = "product";
    @BindView(R.id.product_image_view)
    ImageView imageViewProduct;
    @BindView(R.id.product_name_text_view)
    TextView tvProductName;
    @BindView(R.id.product_price_text_view)
    TextView tvProductPrice;
    @BindView(R.id.product_on_sale_text_view)
    TextView tvProductOnSale;
    @BindView(R.id.product_description_text_view)
    TextView tvProductDescription;
    @BindView(R.id.add_to_cart_button)
    Button btnAddToCart;
    @BindView(R.id.remove_from_cart_button)
    Button btnRemoveFromCart;

    Product product;
    ProductDetails productDetails;
    ProductDetailsFragmentPresenterImpl presenter;
    public ProductDetailsFragment() {

    }

    public static ProductDetailsFragment newInstance(Product product) {
        ProductDetailsFragment fragment = new ProductDetailsFragment();
        Bundle args = new Bundle();
        args.putParcelable(PRODUCT, product);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            product = (Product) getArguments().getParcelable(PRODUCT);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        presenter.onSaveInstantState(outState);
    }

    @Override
    protected void initPresenter() {
        EventBus eventBus = EventBus.getDefault();
        IProductDetailsInteractor interactor = new ProductDetailsInteractor(eventBus, new ProductWebService());
        presenter = new ProductDetailsFragmentPresenterImpl(this, interactor, eventBus);

    }

    @Override
    protected View getView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_product_details, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.onViewCreated(savedInstanceState);
        Product product = (Product) getArguments().getSerializable(PRODUCT);
        presenter.checkIsAlreadyInCart(product.getId());
        presenter.findProductDetails(product);

    }

/*    @Override
    protected View getView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_product_details, container, false);
    }*/

    @Override
    public void setProductDetails(ProductDetails productDetails) {
        if(productDetails != null && isAdded()){
            this.productDetails = productDetails;
            setInitialProductDetails(productDetails);
            tvProductDescription.setText(productDetails.getDescription());

        }

    }
    @Override
    public void setInitialProductDetails(Product product){
        if(product != null && isAdded()) {
            tvProductName.setText(product.getName());
            tvProductPrice.setText(getString(R.string.currency) + product.getPrice());
            if (product.isUnderSale()) {
                tvProductOnSale.setVisibility(View.VISIBLE);
            } else {
                tvProductOnSale.setVisibility(View.GONE);
            }

            if (product.getImgUrl() != null) {
                Uri uri = Uri.parse(product.getImgUrl());
                Picasso.with(getActivity()).load(uri).into(imageViewProduct);
            }
        }
    }

    @Override
    public void showAlreadyAddedToCartMessage() {
        if(isAdded()) {
            Toast.makeText(getActivity(), R.string.already_added_to_cart, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void showAddedToCartMessage() {
        if(isAdded()) {
            Toast.makeText(getActivity(), R.string.added_to_cart_successfully, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void showErrorOnSaveToCartMessage() {
        if(isAdded()) {
            Toast.makeText(getActivity(), R.string.failed_to_add_into_cart, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void showAddToCart() {
        if(isAdded()) {
            btnAddToCart.setVisibility(View.VISIBLE);
            btnRemoveFromCart.setVisibility(View.GONE);
        }
    }

    @Override
    public void showRemoveFromCart() {
        if(isAdded()) {
            btnRemoveFromCart.setVisibility(View.VISIBLE);
            btnAddToCart.setVisibility(View.VISIBLE);
        }
        
    }


    @OnClick(R.id.add_to_cart_button)
    public void onClickAddToCart(){
        presenter.onClickAddToCart(product);

    }

    @OnClick(R.id.remove_from_cart_button)
    public void  onClickRemoveFromCart(){
        presenter.onClickDeteleFromCart(product.getId());
    }

    public ProductDetails getProductDetails() {
        return productDetails;
     /*   if(productDetails != null){
            return productDetails;
        }else{
            ProductDetails productDetails = new ProductDetails();
            productDetails.setPrice(product.getPrice());
            productDetails.setImgUrl(product.getImgUrl());
            productDetails.setName(product.getName());
            productDetails.setId(product.getId());
            productDetails.setCategory(product.getCategory());
            productDetails.setUnderSale(product.isUnderSale());
            return productDetails;
        }*/
    }

    @Override
    public boolean isAttachedView() {
        return isAdded();
    }


    @Override
    public void showProductLoadErrorMessage() {
        if(isAdded()) {
            if(CheckNetworkConnection.isNetworkAvailable(getActivity()) == false){
                Toast.makeText(getActivity(), "No Internet", Toast.LENGTH_SHORT).show();
            }else {
                Toast.makeText(getActivity(), R.string.product_details_load_error_message, Toast.LENGTH_SHORT).show();
            }
        }
    }
}
