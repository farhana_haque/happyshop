package com.sephora.happyshop;

import android.app.Application;
import android.content.Context;

/**
 * Created by Farhana on 5/19/2016.
 */
public class App extends Application {

    private static Context mContext;

    public void onCreate(){
        super.onCreate();
        mContext = this;
    }

    public static Context getContext(){
        return mContext;
    }
}
