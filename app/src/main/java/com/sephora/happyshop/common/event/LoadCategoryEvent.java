package com.sephora.happyshop.common.event;

import com.sephora.happyshop.common.webservice.Model.Category;

import java.util.List;

/**
 * Created by Farhana on 5/23/2016.
 */
public class LoadCategoryEvent {
    private List<Category> categories;

    public LoadCategoryEvent(List<Category> categories) {
        this.categories = categories;
    }

    public List<Category> getCategories() {
        return categories;
    }
}
