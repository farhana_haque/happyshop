package com.sephora.happyshop.common.webservice.response;

import com.google.gson.annotations.SerializedName;
import com.sephora.happyshop.common.webservice.Model.ProductDetails;

import java.io.Serializable;

/**
 * Created by Farhana on 5/19/2016.
 */
public class ProductDetailsResponse implements Serializable{

    @SerializedName("product")
    private ProductDetails product;

    public ProductDetails getProduct() {
        return product;
    }

    public void setProduct(ProductDetails product) {
        this.product = product;
    }
}
