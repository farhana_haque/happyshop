package com.sephora.happyshop.common.callback;

/**
 * Created by Farhana on 5/19/2016.
 */
public interface OnDatabaseDeleteListener {
    void deletedSuccessfully();
    void errorOnDelete();
}
