package com.sephora.happyshop.common.webservice.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Farhana on 5/19/2016.
 */
public class ProductDetails extends Product{
    @SerializedName("description")
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
