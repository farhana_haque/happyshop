package com.sephora.happyshop.common.storage.Constant;

/**
 * Created by Farhana on 5/19/2016.
 */
public class ProductPersistStatusConstants {
    public static final int SAVED_SUCCESSFULLY = 0;
    public static final int ALREADY_EXISTS = 1;
    public static final int ERROR_ON_SAVE = -1;
}
