package com.sephora.happyshop.common.base;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import butterknife.ButterKnife;

public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupLayout();
        ButterKnife.bind(this);
        initView();
    }

    protected abstract void setupLayout();

    protected abstract void initView();
}
