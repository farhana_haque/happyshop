package com.sephora.happyshop.common.callback;

/**
 * Created by Farhana on 5/19/2016.
 */
public interface OnDatabaseSaveListener {
    void addedSuccessfully();
    void errorOnAdd();
    void alreadyAdded();
}
