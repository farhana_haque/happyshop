package com.sephora.happyshop.common.storage.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.sephora.happyshop.common.storage.Constant.DatabaseConstants;

/**
 * Created by farhana.haque on 5/18/2016.
 */
public class DBHelper {

    private final String DATABASE_CREATE_TABLE_PRODUCT = "create table "
            + DatabaseConstants.PRODUCT_TABLE_NAME + " ("
            + DatabaseConstants.PRODUCT_ID + " integer primary key,"
            + DatabaseConstants.SAVED_PRODUCT_TYPE + " text not null, "
            + DatabaseConstants.PRODUCT_NAME + " text not null, "
            + DatabaseConstants.PRODUCT_IMAGE_URL + " text , "
            + DatabaseConstants.PRODUCT_PRICE + " real , "
            + DatabaseConstants.PRODUCT_ON_SALE + " integer"
            + ");";


    private final String TAG = DBHelper.class.getSimpleName();


    /**
     * Singleton object for the DbHelper class
     */
    private final HappyShopDatabaseHelper helper;
    private SQLiteDatabase myDatabase;

    /**
     * Contructor
     *
     * @param context current application context
     */
    public DBHelper(Context context) {
        helper = new HappyShopDatabaseHelper(context, DatabaseConstants.DATABASE_NAME, null, 1);
    }

    /**
     * This method is used to open the database in Writable mode
     *
     * @return instance of the database
     */
    public DBHelper open() {
        try {
            myDatabase = helper.getWritableDatabase();
        } catch (SQLException ex) {
            Log.e(TAG, "::Writable database cudnt be created::" + ex.toString());
            myDatabase = helper.getReadableDatabase();
        }
        return this;
    }

    /**
     * This method is used for closing the database
     */
    public void close() {
        myDatabase.close();
    }

    /**
     * This method is used for inserting values in the database table
     *
     * @param values    contains values to be inserted in the table. this map contains the initial column values for the
     *                  row. The keys should be the column names and the values the
     *                  column values
     * @param tableName name of the table in which row is to be updated
     * @return the row ID of the newly inserted row, or -1 if an error occurred
     */

    public long insertRows(ContentValues values, String tableName) {
        long val = myDatabase.insert(tableName, null, values);
        return val;
    }

    /**
     * This method is used for deleting values in the database table
     *
     * @param tableName name of the table from which contents need to be removed
     * @return the number of rows affected if a whereClause is passed in, 0
     * otherwise. To remove all rows and get a count pass "1" as the
     * whereClause.
     */
    public int emptyTable(String tableName) {
        return myDatabase.delete(tableName, null, null);
    }

    /**
     * This method is used for deleting values in the database table
     *
     * @param tableName Name of the table which needs to be removed
     */
    public void removeTable(String tableName) {
        myDatabase.execSQL("DROP TABLE IF EXISTS " + tableName);
    }

    /**
     * This method is used to update values in the database table
     *
     * @param values    contains values to be updated
     * @param where     contains condition for updating
     * @param tableName name of the which needs to be updated
     * @return number of rows updated
     */
    public int update(ContentValues values, String where, String tableName) {
        int count = myDatabase.update(tableName, values, where, null);
        return count;
    }

    /**
     * This method is used to update values in the database table
     *
     * @param where     contains condition for updating
     * @param tableName name of the which needs to be updated
     * @return number of rows updated
     */
    public int deleteRow(String tableName, String where, String[] whereArgs) {
        int count = myDatabase.delete(tableName, where, whereArgs);
        return count;
    }

    /**
     * This method is used to get the values from the database Table
     *
     * @param tableName name of table from which data is required
     * @return the cursor on the result of the query
     */
    public Cursor getAllValues(String tableName, String selection, String[] selectionArgs) {
        Cursor myResult;
        myResult = myDatabase.query(tableName, null, selection, selectionArgs,
                null, null, null, null);
        return myResult;
    }

    public Cursor rawQuery(String mY_QUERY, String[] selections) {
        return myDatabase.rawQuery(mY_QUERY, selections);
    }

    // Inner class
    private class HappyShopDatabaseHelper extends SQLiteOpenHelper {
        public HappyShopDatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        /**
         * this method is called when the database is created first time
         */
        @Override
        public void onCreate(SQLiteDatabase _db) {
            _db.execSQL(DATABASE_CREATE_TABLE_PRODUCT);

        }

        /**
         * this method is called when the database version is changed
         */
        @Override
        public void onUpgrade(SQLiteDatabase _db, int _oldVersion, int _newVersion) {
            removeTable(DatabaseConstants.PRODUCT_TABLE_NAME);
            onCreate(_db);
        }
    }
}