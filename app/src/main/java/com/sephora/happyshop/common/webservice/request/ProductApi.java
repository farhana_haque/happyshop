package com.sephora.happyshop.common.webservice.request;

import com.sephora.happyshop.common.webservice.response.ProductDetailsResponse;
import com.sephora.happyshop.common.webservice.response.ProductListResponse;

import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;



/**
 * Created by Farhana on 5/18/2016.
 */
public interface ProductApi {
    @GET("/products.json")
    rx.Observable<ProductListResponse> getProducts(
            @Query("category") String category, @Query("page") int page);

    @GET("/products/{id}")
    rx.Observable<ProductDetailsResponse> getProduct(@Path("id") String id);

}
