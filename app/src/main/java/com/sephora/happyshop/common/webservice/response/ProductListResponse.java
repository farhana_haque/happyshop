package com.sephora.happyshop.common.webservice.response;

import com.google.gson.annotations.SerializedName;
import com.sephora.happyshop.common.webservice.Model.Product;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Farhana on 5/18/2016.
 */
public class ProductListResponse implements Serializable{

    @SerializedName("products")
    List<Product> products;

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }
    /* public ProductList productList;

    public ProductList getProductList() {
        return productList;
    }

    public void setProductList(ProductList productList) {
        this.productList = productList;
    }

    public List<Product> getProduct(){
        return productList.products;
    }

    public class ProductList{
        public List<Product> products;
    }*/
}
