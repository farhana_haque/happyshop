package com.sephora.happyshop.common.callback;

/**
 * Created by Farhana on 5/17/2016.
 */
public interface OnDataLoadFinishedListener {
    <T> void onLoadingFinish(T response);
    void onLoadingError();
}
