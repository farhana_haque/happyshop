package com.sephora.happyshop.common.callback;

/**
 * Created by Farhana on 5/20/2016.
 */
public interface OnDatabaseExistsListener {
    void isExists(boolean exists);
}
