package com.sephora.happyshop.common.storage.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.sephora.happyshop.common.storage.Constant.DatabaseConstants;
import com.sephora.happyshop.common.storage.Constant.ProductPersistStatusConstants;
import com.sephora.happyshop.common.webservice.Model.Product;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by farhana.haque on 5/18/2016.
 */
public class DBManager {
    public static final String TAG = DBManager.class.getName();
    private static final Object mutex = new Object();
    private static final int TRUE = 1;
    private static final int FALSE = 0;
    private static DBManager instance = null;
    private final DBHelper db;
    private Context context;

    private DBManager(Context context) {
        db = new DBHelper(context);
        this.context = context;
    }

    private static void createInstance(Context context) {
        if (instance != null) {
            return;
        }
        synchronized (mutex) {
            if (instance == null) {
                instance = new DBManager(context);
            }
        }
    }

    public static DBManager getInstance(Context context) {
        if (null == instance) {
            createInstance(context);
        }
        return instance;
    }

    public void clearTable(String tableName) {
        db.emptyTable(tableName);
    }

    public void openDB() {
        db.open();
    }

    public void closeDB() {
        db.close();
    }

    public int saveProduct(Product product, String type) {
        openDB();
        ContentValues contentValues = getProductContentValues(product, type);
        if(isExistsProduct(product.getId())) {
            closeDB();
            return ProductPersistStatusConstants.ALREADY_EXISTS;
        }
        long value = db.insertRows(contentValues, DatabaseConstants.PRODUCT_TABLE_NAME);
        closeDB();
        return (value == -1) ? ProductPersistStatusConstants.ERROR_ON_SAVE : ProductPersistStatusConstants.SAVED_SUCCESSFULLY;

    }

    public List<Product> getProducts(){
        List<Product> products = new ArrayList<>();
        openDB();
        Cursor cursor = db.getAllValues(DatabaseConstants.PRODUCT_TABLE_NAME, null, null);
        cursor.moveToFirst();
        if(cursor.getCount()> 0) {
            do {
                Product product = new Product();
                long productId = cursor.getLong(cursor.getColumnIndex(DatabaseConstants.PRODUCT_ID));
                String productName = cursor.getString(cursor.getColumnIndex(DatabaseConstants.PRODUCT_NAME));
                double productPrice = cursor.getDouble(cursor.getColumnIndex(DatabaseConstants.PRODUCT_PRICE));
                String productImageUrl = cursor.getString(cursor.getColumnIndex(DatabaseConstants.PRODUCT_IMAGE_URL));
                int productOnSale = cursor.getInt(cursor.getColumnIndex(DatabaseConstants.PRODUCT_ON_SALE));

                product.setId(productId);
                product.setName(productName);
                product.setPrice(productPrice);
                product.setImgUrl(productImageUrl);
                product.setUnderSale((productOnSale == TRUE) ? true : false);
                products.add(product);


            } while (cursor.moveToNext());
        }
        closeDB();
        return  products;
    }

    public Product getProduct(long id) {
        openDB();
        String selection = "((" + DatabaseConstants.PRODUCT_ID + "=?))";
        String[] selectionArgs = new String[]{String.valueOf(id)};
        Cursor cursor = db.getAllValues(DatabaseConstants.PRODUCT_TABLE_NAME, selection, selectionArgs);
        cursor.moveToFirst();
        Product product = null;
        if (cursor.getCount() > 0) {
            long productId = cursor.getLong(cursor.getColumnIndex(DatabaseConstants.PRODUCT_ID));
            String productName = cursor.getString(cursor.getColumnIndex(DatabaseConstants.PRODUCT_NAME));
            double productPrice = cursor.getDouble(cursor.getColumnIndex(DatabaseConstants.PRODUCT_PRICE));
            String productImageUrl = cursor.getString(cursor.getColumnIndex(DatabaseConstants.PRODUCT_IMAGE_URL));
            int productOnSale = cursor.getInt(cursor.getColumnIndex(DatabaseConstants.PRODUCT_ON_SALE));
            product = new Product();
            product.setId(productId);
            product.setName(productName);
            product.setPrice(productPrice);
            product.setImgUrl(productImageUrl);
            product.setUnderSale((productOnSale == TRUE) ? true : false);



        }
        closeDB();
        return product;
    }

    private boolean isExistsProduct(long id){
        String selection = "((" + DatabaseConstants.PRODUCT_ID + "=?))";
        String[] selectionArgs = new String[]{String.valueOf(id)};
        Cursor cursor = db.getAllValues(DatabaseConstants.PRODUCT_TABLE_NAME, selection, selectionArgs);
        cursor.moveToFirst();
        return cursor.getCount() > 0;
    }

    public boolean checkProductExists(long id){
        openDB();
        boolean isProductExists = isExistsProduct(id);
        closeDB();
        return isProductExists;
    }

    public boolean deleteProducts(long id){
        openDB();
        String selection = "(("+ DatabaseConstants.PRODUCT_ID+ "=?))";
        String[] selectionArgs = new String[]{String.valueOf(id)};
        boolean isDeleted = db.deleteRow(DatabaseConstants.PRODUCT_TABLE_NAME, selection, selectionArgs) > 0;
        closeDB();
        return isDeleted;
    }

    private ContentValues getProductContentValues(Product product, String type) {
        ContentValues cv = new ContentValues();
        cv.put(DatabaseConstants.PRODUCT_ID, product.getId());
        cv.put(DatabaseConstants.PRODUCT_NAME, product.getName());
        cv.put(DatabaseConstants.PRODUCT_IMAGE_URL, product.getImgUrl());
        cv.put(DatabaseConstants.PRODUCT_PRICE, product.getPrice());
        int underSale = (product.isUnderSale() == true) ? TRUE : FALSE;
        cv.put(DatabaseConstants.PRODUCT_ON_SALE, underSale);
        cv.put(DatabaseConstants.SAVED_PRODUCT_TYPE, type);
        return cv;
    }
}
