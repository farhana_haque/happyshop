package com.sephora.happyshop.common.callback;

/**
 * Created by farhana.haque on 5/18/2016.
 */
public interface OnItemClickListener {
    <T> void onItemClick(T item);
}
