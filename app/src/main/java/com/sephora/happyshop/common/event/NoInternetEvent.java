package com.sephora.happyshop.common.event;

/**
 * Created by Farhana on 5/23/2016.
 */
public class NoInternetEvent {
    private String message;

    public NoInternetEvent(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
