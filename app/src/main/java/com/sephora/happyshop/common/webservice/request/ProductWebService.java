package com.sephora.happyshop.common.webservice.request;

import retrofit.RestAdapter;

/**
 * Created by Farhana on 5/18/2016.
 */
public class ProductWebService {
    private static final String BASE_URL = "http://sephora-mobile-takehome-apple.herokuapp.com/api/v1";
    private ProductApi productApi;

    public ProductWebService(){
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(BASE_URL)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        productApi = restAdapter.create(ProductApi.class);

    }

    public ProductApi getProductApi() {
        return productApi;
    }
}
