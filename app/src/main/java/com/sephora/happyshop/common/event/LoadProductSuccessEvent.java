package com.sephora.happyshop.common.event;

import com.sephora.happyshop.common.webservice.Model.Product;

import java.util.List;

/**
 * Created by Farhana on 5/23/2016.
 */
public class LoadProductSuccessEvent {
    List<Product> products;

    public LoadProductSuccessEvent(List<Product> products) {
        this.products = products;
    }

    public List<Product> getProducts() {
        return products;
    }
}
