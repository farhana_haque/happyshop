package com.sephora.happyshop.common.webservice.Model;

/**
 * Created by Farhana on 5/16/2016.
 */
public class Category {
    private String name;

    public Category(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
