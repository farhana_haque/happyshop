package com.sephora.happyshop.common.event;

/**
 * Created by Farhana on 5/23/2016.
 */
public class ProductDetailsLoadErrorEvent {
    private String message;

    public ProductDetailsLoadErrorEvent(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
