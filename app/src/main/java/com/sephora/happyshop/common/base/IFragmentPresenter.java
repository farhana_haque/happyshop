package com.sephora.happyshop.common.base;

import android.os.Bundle;

/**
 * Created by Farhana on 5/17/2016.
 */
public interface IFragmentPresenter {
    void onCreate(Bundle savedInstanceState);
    void onSaveInstantState(Bundle outState);
    void onViewCreated(Bundle bundle);
}
