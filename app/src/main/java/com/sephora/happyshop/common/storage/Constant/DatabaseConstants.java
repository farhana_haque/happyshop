package com.sephora.happyshop.common.storage.Constant;

/**
 * Created by farhana.haque on 5/18/2016.
 */
public class DatabaseConstants {
    public static final String DATABASE_NAME = "happy_shop.db";
    public static final String PRODUCT_TABLE_NAME = "products";
    public static final String ID = "id";
    public static final String PRODUCT_ID = "product_id";
    public static final String PRODUCT_NAME = "product_name";
    public static final String PRODUCT_IMAGE_URL = "product_image_url";
    public static final String PRODUCT_PRICE = "product_price";
    public static final String PRODUCT_ON_SALE = "product_on_sale";
    public static final String SAVED_PRODUCT_TYPE = "saved_product_type";

}
