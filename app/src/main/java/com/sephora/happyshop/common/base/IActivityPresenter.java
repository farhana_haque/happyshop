package com.sephora.happyshop.common.base;

import android.os.Bundle;

/**
 * Created by Farhana on 5/18/2016.
 */
public interface IActivityPresenter {
    void onCreate(Bundle savedInstanceState);
}
