package com.sephora.happyshop.common.event;

import com.sephora.happyshop.common.webservice.Model.ProductDetails;

/**
 * Created by Farhana on 5/23/2016.
 */
public class ProductDetailsLoadSuccessEvent {
    private ProductDetails productDetails;

    public ProductDetailsLoadSuccessEvent(ProductDetails productDetails) {
        this.productDetails = productDetails;
    }

    public ProductDetails getProductDetails() {
        return productDetails;
    }
}
