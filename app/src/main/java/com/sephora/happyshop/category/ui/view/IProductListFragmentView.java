package com.sephora.happyshop.category.ui.view;

import com.sephora.happyshop.common.webservice.Model.Product;

import java.util.List;

/**
 * Created by Farhana on 5/18/2016.
 */
public interface IProductListFragmentView {
    void showLoadMore();
    void hideLoadMore();
    void noMoreData();
    void setData(List<Product> products);
    void addData(List<Product> products);
    void resetData();
    void showNoDataLayout();
    void showDataLoadErrorMessage();
    void navagateToProductDetails(int itemIndex);
    boolean isAttachedView();
    void showNoInternetMessage();
    boolean isConnectedToInternet();
    List<Product> getProducts();
}
