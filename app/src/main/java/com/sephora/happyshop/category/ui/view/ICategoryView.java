package com.sephora.happyshop.category.ui.view;

import android.os.Bundle;

/**
 * Created by Farhana on 5/18/2016.
 */
public interface ICategoryView {
    void setProductListFragment(Bundle savedInstanceState);
}
