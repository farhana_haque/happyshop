package com.sephora.happyshop.category.presenter;

import android.os.Bundle;
import android.util.Log;

import com.sephora.happyshop.category.interactor.IProductListInteractor;
import com.sephora.happyshop.category.ui.view.IProductListFragmentView;
import com.sephora.happyshop.common.event.LoadProductErrorEvent;
import com.sephora.happyshop.common.event.LoadProductSuccessEvent;
import com.sephora.happyshop.common.webservice.Model.Product;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Farhana on 5/18/2016.
 */
public class ProductListPresenterImpl implements IProductListPresenter {
    private static final String TAG = IProductListPresenter.class.getSimpleName();
    private static final String PRODUCTS = "products";
    private static final String CURRENT_PAGE = "current_page";
    private static final String MAX_PAGE_REACHED = "max_page_reached";
    //String category;
    private int currentPage = 1;
    //private int currentRequestedPage;
    boolean maxPageReached = false;


    IProductListFragmentView productListFragmentView;
    IProductListInteractor productListInteractor;
    EventBus eventBus;
    public ProductListPresenterImpl(IProductListFragmentView productListFragmentView, IProductListInteractor interactor, EventBus eventBus){
        this.productListFragmentView = productListFragmentView;
        this.eventBus = eventBus;
        this.productListInteractor = interactor;
       // productListInteractor = new ProductListInteractorImpl(new ProductWebService());

    }

    @Override
    public void findProducts(String category) {
        if(productListFragmentView.isConnectedToInternet() == false){
            showNoInternet();
            return;
        }
        if(maxPageReached == false) {
            //productListFragmentView.showLoadMore();
            showLoader();
            eventBus.register(this);
            productListInteractor.findProducts(category, currentPage++);
            Log.d(TAG, "Current requested page "+currentPage);
        }else{
            hideLoader();
        }
    }

    @Override
    public void productItemClick(int itemIndex) {
        productListFragmentView.navagateToProductDetails(itemIndex);
    }


    @Override
    public void onViewCreated(Bundle savedInstanceState) {
        if(savedInstanceState != null){
            List<Product> products = (List<Product>) savedInstanceState.getSerializable(PRODUCTS);
            addProducts(products);
            currentPage = savedInstanceState.getInt(CURRENT_PAGE);
            maxPageReached = savedInstanceState.getBoolean(MAX_PAGE_REACHED);
            Log.d(TAG, "restored requested page "+currentPage);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

    }

    @Override
    public void onSaveInstantState(Bundle bundle) {
        bundle.putSerializable(PRODUCTS, new ArrayList<>(productListFragmentView.getProducts()));
        bundle.putInt(CURRENT_PAGE, currentPage);
        bundle.putBoolean(MAX_PAGE_REACHED, maxPageReached);

    }

/*    public IProductListInteractor getProductListInteractor() {
        return productListInteractor;
    }*/

    /*
    @Override
    public <T> void onLoadingFinish(List<T> items) {
        productListFragmentView.hideLoadMore();
        productListFragmentView.addData((List<Product>) items);
        currentPage++;
        if(items.size() == 0) {
            maxPageReached = true;
            productListFragmentView.hideLoadMore();
        }


    }*/

/*    @Override
    public <T> void onLoadingFinish(T response) {
        List<Product> products = (List<Product>) response;
        productListFragmentView.hideLoadMore();
        productListFragmentView.addData(products);
        currentPage++;
        if(products.size() == 0) {
            maxPageReached = true;
            //productListFragmentView.hideLoadMore();
        }
    }*/

    @Subscribe
    void onLoadFinish(LoadProductSuccessEvent loadProductSuccessEvent){
        eventBus.unregister(this);
        Log.d(TAG, "Served requested page "+currentPage);
        addProducts(loadProductSuccessEvent.getProducts());
        //currentPage++;


    }
    private void addProducts(List<Product> products){
        if(productListFragmentView.isAttachedView()){
            productListFragmentView.hideLoadMore();
            productListFragmentView.addData(products);
            if(products.size() == 0) {
                maxPageReached = true;
                //productListFragmentView.hideLoadMore();
            }
        }
    }

    @Subscribe
    void onLoadingError(LoadProductErrorEvent loadProductErrorEvent){
        eventBus.unregister(this);
        if(productListFragmentView.isAttachedView()){
            productListFragmentView.showDataLoadErrorMessage();
        }
    }


    void showNoInternet(){
        if(productListFragmentView.isAttachedView()){
            productListFragmentView.hideLoadMore();
            productListFragmentView.showNoInternetMessage();
        }
    }

/*    @Override
    public void onLoadingError() {
        productListFragmentView.showDataLoadErrorMessage();

    }*/

    private void showLoader(){
        if(productListFragmentView.isAttachedView()){
            productListFragmentView.showLoadMore();
        }
    }

    private void hideLoader(){
        if(productListFragmentView.isAttachedView()) {
            productListFragmentView.hideLoadMore();
        }
    }
}
