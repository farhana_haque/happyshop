package com.sephora.happyshop.category.interactor;

import android.util.Log;

import com.sephora.happyshop.common.event.LoadProductErrorEvent;
import com.sephora.happyshop.common.event.LoadProductSuccessEvent;
import com.sephora.happyshop.common.webservice.request.ProductWebService;
import com.sephora.happyshop.common.webservice.response.ProductListResponse;

import org.greenrobot.eventbus.EventBus;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Farhana on 5/18/2016.
 */
public class ProductListInteractorImpl implements IProductListInteractor {
    //EventBus eventBus = EventBus.getDefault();
    EventBus eventBus;
    private final String TAG = "ProductListInteractor";
    ProductWebService productWebService;
    public ProductListInteractorImpl(ProductWebService productWebService, EventBus eventBus){
        //productWebService = new ProductWebService();
        this.eventBus = eventBus;
        this.productWebService = productWebService;
    }
    @Override
    public void findProducts(String category, int page) {
        productWebService.getProductApi()
                .getProducts(category,page)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ProductListResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, e.getMessage());
                        eventBus.post(new LoadProductErrorEvent());
                        //finishedListener.onLoadingError();

                    }

                    @Override
                    public void onNext(ProductListResponse productListResponse) {
                        LoadProductSuccessEvent event = new LoadProductSuccessEvent(productListResponse.getProducts());
                        eventBus.post(event);
                        //finishedListener.onLoadingFinish(productListResponse.getProducts());
                    }
                });


    }

   /* public ProductWebService getProductWebService() {
        return productWebService;
    }*/
}
