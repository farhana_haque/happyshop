package com.sephora.happyshop.category.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.sephora.happyshop.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by farhana.haque on 5/18/2016.
 */
public class ProductRecyclerViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.product_name_text_view)
    public TextView tvProductName;
    @BindView(R.id.product_on_sale_text_view)
    public TextView tvProductOnSale;
    @BindView(R.id.product_price_text_view)
    public TextView tvProductPrice;
    @BindView(R.id.product_image_view)
    public ImageView imageViewProduct;

    public ProductRecyclerViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        //tvProductName = (TextView) itemView.findViewById(R.id.product_name_text_view);
        //tvProductPrice = (TextView) itemView.findViewById(R.id.product_price_text_view);
        //tvProductOnSale = (TextView) itemView.findViewById(R.id.product_on_sale_text_view);
        //imageViewProduct = (ImageView) itemView.findViewById(R.id.product_image_view);
    }
}
