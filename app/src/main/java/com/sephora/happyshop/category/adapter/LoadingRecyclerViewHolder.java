package com.sephora.happyshop.category.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.sephora.happyshop.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by farhana.haque on 5/18/2016.
 */
public class LoadingRecyclerViewHolder extends RecyclerView.ViewHolder{
    @BindView(R.id.loading_progress_bar)
    ProgressBar progressBar;
    public LoadingRecyclerViewHolder(View itemView) {

        super(itemView);
        ButterKnife.bind(this, itemView);
        //progressBar = (ProgressBar) itemView.findViewById(R.id.loading_progress_bar);
    }
}
