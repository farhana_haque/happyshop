package com.sephora.happyshop.category.interactor;

/**
 * Created by Farhana on 5/18/2016.
 */
public interface IProductListInteractor {
    void findProducts(String category, int page);
}
