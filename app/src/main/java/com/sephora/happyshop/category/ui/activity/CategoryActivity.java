package com.sephora.happyshop.category.ui.activity;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.sephora.happyshop.R;
import com.sephora.happyshop.category.ui.fragment.ProductListFragment;
import com.sephora.happyshop.common.base.BaseActivity;

import butterknife.BindView;

public class CategoryActivity extends BaseActivity {
    public static final String EXTRA_CATEGORY = "category";
    @BindView(R.id.toolbar) Toolbar toolbar;




    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState == null){
            ProductListFragment fragment = ProductListFragment.newInstance(getExtraCategory());
            getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, fragment).commit();
        }


    }

    @Override
    protected void setupLayout() {
        setContentView(R.layout.activity_category);
    }

    @Override
    protected void initView() {
        initToolBar();
    }

    protected void initToolBar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getExtraCategory());


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private String getExtraCategory(){
        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            return bundle.getString(EXTRA_CATEGORY);
        }
        return null;
    }
}
