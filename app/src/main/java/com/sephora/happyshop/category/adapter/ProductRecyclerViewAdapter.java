package com.sephora.happyshop.category.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sephora.happyshop.R;
import com.sephora.happyshop.common.callback.OnLoadMoreListener;
import com.sephora.happyshop.common.webservice.Model.Product;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by farhana.haque on 5/18/2016.
 */
public class ProductRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    public static final int VIEW_TYPE_ITEM = 0;
    public static final int VIEW_TYPE_LOADING = 1;
    List<Product> products = new ArrayList<>();
    private OnLoadMoreListener onLoadMoreListener;

    private boolean isLoading;
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    Context context;

    public ProductRecyclerViewAdapter(Context context){
        this.context = context;
    }


    public void setOnLoadMoreListener(OnLoadMoreListener loadMoreListener, RecyclerView recyclerView){
        this.onLoadMoreListener = loadMoreListener;
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (onLoadMoreListener != null) {
                        onLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == VIEW_TYPE_ITEM){
            View view = LayoutInflater.from(context).inflate(R.layout.product_list_item_layout, parent, false);
            return new ProductRecyclerViewHolder(view);
        }else if(viewType == VIEW_TYPE_LOADING){
            View view = LayoutInflater.from(context).inflate(R.layout.loading_item_layout, parent, false);
            return new LoadingRecyclerViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof ProductRecyclerViewHolder){
            Product product = products.get(position);
            ProductRecyclerViewHolder productRecyclerViewHolder = (ProductRecyclerViewHolder) holder;
            productRecyclerViewHolder.tvProductName.setText(product.getName());
            productRecyclerViewHolder.tvProductPrice.setText(context.getString(R.string.currency)+product.getPrice());
            if(product.isUnderSale()){
                productRecyclerViewHolder.tvProductOnSale.setVisibility(View.VISIBLE);
            }else{
                productRecyclerViewHolder.tvProductOnSale.setVisibility(View.GONE);
            }

            if(product.getImgUrl() != null) {
                Uri uri = Uri.parse(product.getImgUrl());
                Picasso.with(context).load(uri).into(productRecyclerViewHolder.imageViewProduct);
            }

        }

    }

    @Override
    public int getItemCount() {
        return products == null ? 0 : products.size();
    }

    @Override
    public int getItemViewType(int position) {
        return products.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    public void setLoadingStatus(boolean loading){
        this.isLoading = loading;

    }

    public void setProducts(List<Product> products){
        this.products = products;
    }

    public void addProducts(List<Product> products) {
        this.products.addAll(products);
    }

    public void resetData() {
        this.products.clear();
    }

    public void addProduct(Product product){
        products.add(product);
    }

    public void remove(int index){
        if(index >=0 && index < getItemCount() ) {
            products.remove(index);
        }
    }

    public Product getItem(int itemIndex) {
        if(itemIndex >= 0 &&  itemIndex < getItemCount())
            return products.get(itemIndex);
        return null;

    }

    public List<Product> getProducts() {
        return products;
    }
}
