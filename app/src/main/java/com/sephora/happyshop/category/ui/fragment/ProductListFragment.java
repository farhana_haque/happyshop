package com.sephora.happyshop.category.ui.fragment;

import android.content.Intent;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.sephora.happyshop.R;
import com.sephora.happyshop.category.interactor.ProductListInteractorImpl;
import com.sephora.happyshop.common.adapter.RecyclerItemClickListener;
import com.sephora.happyshop.common.base.BaseFragment;
import com.sephora.happyshop.common.callback.OnLoadMoreListener;
import com.sephora.happyshop.category.presenter.ProductListPresenterImpl;
import com.sephora.happyshop.category.adapter.ProductRecyclerViewAdapter;
import com.sephora.happyshop.category.ui.view.IProductListFragmentView;
import com.sephora.happyshop.common.webservice.Model.Product;
import com.sephora.happyshop.common.webservice.request.ProductWebService;
import com.sephora.happyshop.common.webservice.util.CheckNetworkConnection;
import com.sephora.happyshop.product.ui.activity.ProductDetailsActivity;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.BindView;

/**
 * A placeholder fragment containing a simple view.
 */
public class ProductListFragment extends BaseFragment implements IProductListFragmentView {
    private static final String TAG = ProductListFragment.class.getSimpleName();
    public static final String EXTRA_CATEGORY = "category";
    ProductListPresenterImpl presenter;
    String category;

    @BindView(R.id.recycler_view_product_list)
    RecyclerView recyclerView;

    private GridLayoutManager gridLayoutManager;
    private ProductRecyclerViewAdapter productRecyclerViewAdapter;


    public ProductListFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        category = getArguments().getString(EXTRA_CATEGORY);
        presenter.onCreate(savedInstanceState);
    }

    public static ProductListFragment newInstance(String category) {
        ProductListFragment fragment = new ProductListFragment();
        Bundle args = new Bundle();
        args.putString(EXTRA_CATEGORY, category);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected void initPresenter() {
        presenter = new ProductListPresenterImpl(this, new ProductListInteractorImpl(new ProductWebService(), EventBus.getDefault()), EventBus.getDefault());
    }

    @Override
    protected View getView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_product_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        gridLayoutManager = new GridLayoutManager(getActivity(), getResources().getInteger(R.integer.max_product_in_grid));
        recyclerView.setLayoutManager(gridLayoutManager);
        productRecyclerViewAdapter = new ProductRecyclerViewAdapter(getActivity());
        recyclerView.setAdapter(productRecyclerViewAdapter);
        productRecyclerViewAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                Log.d(TAG, "onLoadMore");
                presenter.findProducts(category);

            }
        }, recyclerView);

        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                switch (productRecyclerViewAdapter.getItemViewType(position)) {
                    case ProductRecyclerViewAdapter.VIEW_TYPE_ITEM:
                        return 1;
                    case ProductRecyclerViewAdapter.VIEW_TYPE_LOADING:
                        return getResources().getInteger(R.integer.max_product_in_grid); //number of columns of the grid
                    default:
                        return -1;
                }
            }
        });

        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Log.d(TAG, "On item click " + position);
                presenter.productItemClick(position);
            }
        }));

      /*  ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                Log.d(TAG, "On item click " + position);
                presenter.productItemClick(position);
            }
        });*/
        presenter.onViewCreated(savedInstanceState);
        presenter.findProducts(category);

    }


    @Override
    public void showLoadMore() {
        Log.d(TAG, "show load more");
        hideLoadMore();
        productRecyclerViewAdapter.addProduct(null);
        //productRecyclerViewAdapter.notifyItemInserted(productRecyclerViewAdapter.getItemCount() -1);
        productRecyclerViewAdapter.setLoadingStatus(true);
        productRecyclerViewAdapter.notifyDataSetChanged();

    }

    @Override
    public void hideLoadMore() {
        Log.d(TAG, "hide load more");
        productRecyclerViewAdapter.setLoadingStatus(false);
        int loadingItemIndex = productRecyclerViewAdapter.getItemCount() - 1;
        Log.d(TAG, "load more item index " + loadingItemIndex);
        if (loadingItemIndex > -1 && productRecyclerViewAdapter.getItemViewType(loadingItemIndex) == ProductRecyclerViewAdapter.VIEW_TYPE_LOADING) {
            Log.d(TAG, "removing load more");
            productRecyclerViewAdapter.remove(loadingItemIndex);
            productRecyclerViewAdapter.notifyDataSetChanged();
            //productRecyclerViewAdapter.notifyItemRemoved(productRecyclerViewAdapter.getItemCount());
        }

    }

    @Override
    public void noMoreData() {

    }

    @Override
    public void setData(List<Product> products) {
        if (isAdded()) {
            productRecyclerViewAdapter.setProducts(products);
            productRecyclerViewAdapter.notifyDataSetChanged();
        }


    }

    @Override
    public void addData(List<Product> products) {
        if (isAdded()) {
            productRecyclerViewAdapter.addProducts(products);
            productRecyclerViewAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        presenter.onSaveInstantState(outState);
    }

    @Override
    public void resetData() {
        productRecyclerViewAdapter.resetData();
        productRecyclerViewAdapter.notifyDataSetChanged();
    }

    @Override
    public void showNoDataLayout() {

    }

    @Override
    public void showDataLoadErrorMessage() {
        Toast.makeText(getActivity(), getString(R.string.product_load_error_message), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void navagateToProductDetails(int itemIndex) {
        Log.d(TAG, "Navigate to product details " + itemIndex);
        if (itemIndex < 0) {
            itemIndex = 0;
        }
        Product product = productRecyclerViewAdapter.getItem(itemIndex);

        Intent intent = new Intent(getActivity(), ProductDetailsActivity.class);
       // intent.putExtra(ProductDetailsActivity.PRODUCT, product);
        intent.putExtra(ProductDetailsActivity.PRODUCT, (Parcelable) product);
        startActivity(intent);

    }

    @Override
    public boolean isAttachedView() {
        return isAdded();
    }

    @Override
    public void showNoInternetMessage() {
        Toast.makeText(getActivity(), "No Internet", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean isConnectedToInternet() {
        return CheckNetworkConnection.isNetworkAvailable(getActivity());
    }

    @Override
    public List<Product> getProducts() {
        return productRecyclerViewAdapter.getProducts();
    }
}
