package com.sephora.happyshop.category.presenter;

import com.sephora.happyshop.common.base.IFragmentPresenter;

/**
 * Created by Farhana on 5/18/2016.
 */
public interface IProductListPresenter extends IFragmentPresenter {
    void findProducts(String category);
    void productItemClick(int itemIndex);
}
