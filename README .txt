How to build:
Import project from android studio. If some update missing update the missing plugins.
If needed add gradle wrapper.

How the code is structed:

This android project follows MVP (Model, View, Presenter) design pattern.

The MVP pattern allows separate the presentation layer from the logic, so that everything about how 
the interface works is separated from how we represent it on screen. 
Ideally the MVP pattern would achieve that same logic might have completely different and interchangeable views.

For three view I used three package.
Package one deal with home view, package two category and package three deal with product details view. All common things are under package common.

What Imporvements needed:
Configuration change is not handled.
Test case is not included.